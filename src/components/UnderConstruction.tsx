import React from 'react'
import { Helmet } from "react-helmet"

import './UnderConstruction.css'

let hostname: String | undefined =  process.env.REACT_APP_HOSTNAME || window.location.hostname;

export const UnderConstruction = () => {

    return (
        <>
        <Helmet>
            <title>Under construction on {hostname}</title>
        </Helmet>
        <main>
            <h1>{hostname}</h1>
            <p>Our website is under construction.</p>
        </main>
        </>
    );
}

export default UnderConstruction;
